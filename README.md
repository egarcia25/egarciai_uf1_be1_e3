# eGarciai_UF1_Be1_E3

### Exercici 1: Missatge benvinguda- Escriu un missatge de benvinguda per pantalla
![EXERCICI1](exercici1.PNG)

### Exercici 2: Tipus simples- Escriu un enter, un real, i un caràcter per pantalla
![EXERCICI2](exercici2.PNG)

### Exercici 3: Suma enters- Demana dos enters per teclat i mostra la suma per pantalla
![EXERCICI3](exercici3.PNG)

### Exercici 4: Tipus- Indica el tipus de dades que correspon en cada cas:
#### Edat
L'edat es un numero enter.(int)
#### Temperatura 
La temperatura es un nombre real.(double)
#### La resposta a la pregunta: Ets solter (s/n)? 
La resposta es un caracter.(char)
#### El resultat d’avaluar la següent expressió: (5>6 o 4<=8) 
El resultat es un booleà.(bool)
#### El teu nom.
El meu nom es un Array.(char[n])

### Exercici 5: Números- Mostrar per pantalla els 5 primers nombres naturals.
![EXERCICI5](exercici5.PNG)

### Exercici 6: Suma- Mostrar per pantalla la suma i el producte dels 5 primers nombres naturals
![EXERCICI6](exercici6.PNG)

### Exercici 7: Intercanvi- Demana dos enters i intercanvia els valors de les variables
![EXERCICI7](exercici7.PNG)

### Exercici 8: Pantalla de menú- Mostrar per pantalla el següent menú:
![EXERCICI8](exercici8.PNG)

### Exercici 9: Pantalla de menú- Mostra per pantalla el següent menú:
![EXERCICI9](exercici9.PNG)

### Exercici 10: Càlcul del capital final en interès simple- Feu un programa que resolgui el següent enunciat:

![EXERCICI10](exercici10.png)

### Exercici 11: Càlcul del Capital final en interès compost- Feu un programa que resolgui el següent enunciat:
![EXERCICI11](exercici11.PNG)

### Exercici 12: La travessa- Feu un programa que permeti generar una travessa de forma aleatòria.
![EXERCICI12_1](ex12_1_1.PNG)

![EXERCICI12_2](ex12_1_2.PNG)

![EXERCICI12_3](ex12_1_3.PNG)

![EXERCICI12_4](ex12_2.PNG)

### Exercici 13: Joc dels daus- Feu un programa que permeti simular el resultat de tirar un dau.
![EXERCICI13](exercici13.PNG)
